/**
 * Задание - Трансформеры и ООП
 *
 * 1. Создать класс Transformer со свойствами name и health (по умолчанию
 * имеет значение 100) и методом attack()
 * 2. Создать класс Autobot, который наследуется от класса Transformer.
 * - Имеет свойсто weapon, т.к. автоботы сражаются с использованием оружия.
 * - Конструктор класса принимает 2 параметра: имя и оружее (Экземпляр класса
 * Weapon).
 * - Метод attack возвращает результат использования оружия weapon.fight()
 * 3. Создать класс Deceptikon который наследуется от класса Transformer.
 * - Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. Зато
 * они могут иметь разное количество здоровья.
 * - Конструктор класса принимает 2 параметра: имя и количество здоровья health
 * - Метод attack возвращает характеристики стандартного вооружения: { damage: 5, speed: 1000 }
 * 4. Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон
 * и speed - скорость атаки. Имеет 1 метод fight, который возвразает характеристики
 * оружия в виде { damage: 5, speed: 1000 }
 * 5. Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }
 * 6. Создать 1 десептикона с именем Megatron и показателем здоровья 10000
 * 7. Посмотреть что происходит при вызове метода atack() у траснформеров разного типа,
 * посмотреть сигнатуры классов
 * 8. ДЗ: сколько нужно автоботов чтобы победить Мегатрона если параметр speed в оружии это
 * количество милсекунд до следующего удара? Реализовать симуляцию боя. Реализовать визуализацию
 * системы реального времени.
 */

class Transformer{
    constructor(name, health = 100) {
        this.name = name;
        this.health = health;
    }

    attack(){}
    hit(fight){
        this.health = this.health - fight.damage * 1000/fight.speed;
    }
}

class Autobot extends Transformer{
    constructor(name, weapon) {
        super(name);
        this.weapon = weapon;
    }

    attack(){
        return this.weapon.fight();
    }
}

class Deceptikon extends Transformer{
    constructor(name, health) {
        super(name, health);
    }

    attack() {
        return { damage: 5, speed: 1000};
    }
}

class Weapon{
    constructor(damage, speed) {
        this.damage = damage;
        this.speed = speed;
    }

    fight(){
        return {
            damage: this.damage,
            speed: this.speed
        }
    }
}

class Arena{
    constructor(side1, side2, visualiser) {
        this.side1 = side1;
        this.side2 = side2;
        this.visualiser = visualiser;
    }

    start(){
        const timer = setInterval(
            () => {
                this.side1.forEach(bot => this.side2[0].hit(bot.attack()));
                this.side2 = this.side2.filter(bot => bot.health > 0);
                if (this.side2.length === 0) {
                    alert('Side1 won!');
                    clearInterval(timer);
                }

                this.side2.forEach(bot => this.side1[0].hit(bot.attack()));
                this.side1 = this.side1.filter(bot => bot.health > 0);
                if (this.side1.length === 0){
                    alert('Side2 won!');
                    clearInterval(timer);
                }

                this.visualiser.render(
                    this.side1.map(bot => bot.health),
                    this.side2.map(bot => bot.health)
                )
            }, 1000
        )
    }
}

class Visualizer {
    render(hps1, hps2) {
        const side1 = document.querySelector('.arena-side-1');
        side1.innerHTML = hps1.map(
            health => '<div class="bot"><span></span></div>'
        ).join('');
        for(let i = 0; i < hps1.length; i++){
            side1.querySelectorAll('span')[i].textContent = hps1[i];
        }

        const side2 = document.querySelector('.arena-side-2');
        side2.innerHTML = hps2.map(
            health => '<div class="bot"><span></span></div>'
        ).join('');
        for(let i = 0; i < hps2.length; i++){
            side2.querySelectorAll('span')[i].textContent = hps2[i];
        }
    }
}

const arena = new Arena(
    [
        new Autobot('OptimusPrime', new Weapon(100, 500)),
        new Autobot('OptimusPrime', new Weapon(100, 1000)),
        new Autobot('OptimusPrime', new Weapon(100, 2000)),
    ],
    [
        new Deceptikon('Megatron', 10000),
    ],
    new Visualizer
)

arena.start();